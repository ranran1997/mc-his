﻿<%@ page language="C#" masterpagefile="../Commons/Main.master" autoeventwireup="true" inherits="BI_Report, App_Web_danbqqrk" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
    
      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            综合报表
            <asp:Label ID="lblNav" runat="server"></asp:Label>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">综合报表</li>
          </ol>
        </section>
          
        <section class="content">

          <div class="row">
            <div class="col-xs-12">
              <div class="box">

                <div class="box-body table-responsive no-padding no-print">
                    
                    <div style="padding:10px 0px 20px 0px; border-top:solid 1px #F4F4F4;">
                        
                        <table class="tableMain" style="min-width:400px !important">
                            
                              <tr>
                                  <td class="tableMain-title" style="text-align:center; width:80px">
                                      <label><asp:Label ID="Label8" runat="server" Text="时间段"></asp:Label></label>
                                  </td>
                                  <td>
                                    <table class="table2">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control" onfocus="WdatePicker()" AutoComplete="off" Width="120px"></asp:TextBox>
                                            </td>
                                            <td>-</td>
                                            <td>
                                                <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" onfocus="WdatePicker()" AutoComplete="off" Width="120px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                  </td>
                              </tr>

                        </table>

                    </div>
                    
                      <div class="box-footer">
                          <asp:Button ID="btnSave" runat="server" Text="计算" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                          <asp:Button ID="btnRefresh" runat="server" Text="重置" CssClass="btn btn-success" onclick="btnRefresh_Click"></asp:Button>
                          <asp:HyperLink ID="hlPrint" runat="server" CssClass="btn btn-default pull-right" NavigateUrl="javascript:McPrint();"><i class="fa fa-print"></i> 打印</asp:HyperLink>
                      </div>

                </div>
                
                <div class="box-body table-responsive no-padding">
                    
                    <div style="padding:20px; border-top:solid 1px #eee;">

                        <div id="OrderDiv" runat="server"></div>

                        <hr />
                        
                        <div id="ServiceDiv" runat="server"></div>
                        
                        <hr />
                        
<%--                        <div id="ProjectDiv" runat="server"></div>
                        
                        <hr />
                        
                        <div id="ChanceDiv" runat="server"></div>
                        
                        <hr />--%>
                        
                        <div id="CampaignDiv" runat="server"></div>

                    </div>

                </div>
                
              </div>
            </div>
          </div>

        </section>

    </div>
    
</asp:Content>