﻿<%@ page language="C#" autoeventwireup="true" inherits="Booking_Touch_Dashboard_Setting, App_Web_0ed0zj05" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
</head>

<body class="body">
    
    <header>

        <div class="topbar">
            <a href="Default.aspx" class="back_btn"><i class="icon iconfont">&#xf0115;</i></a>
            <a href="javascript:;" class="top_home"></a>
            <h1 class="page_title"><asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>

    </header>

    <form id="form1" runat="server">
    
        <table class="edittb">
            <tr>
                <td class="editname">姓名<span class="must">*</span></td>
                <td>
                    <asp:TextBox ID="txtFullName" runat="server" CssClass="edittxt" autocomplete="off" placeholder="请输入真实姓名"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="editname">性别</td>
                <td>
                    <asp:DropDownList ID="ddlSex" runat="server" CssClass="ddl">
                        <asp:ListItem Text="男" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="女" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="editname">年龄</td>
                <td>
                    <input id="txtAge" runat="server" class="edittxt" autocomplete="off" placeholder="请输入年龄" />
                </td>
            </tr>
            <tr>
                <td class="editname">手机号码<span class="must">*</span></td>
                <td>
                    <input id="txtCellPhone" runat="server" class="edittxt" autocomplete="off" placeholder="请输入手机号码" />
                </td>
            </tr>
            <tr>
                <td class="editname">身份证号</td>
                <td>
                    <asp:TextBox ID="txtIDNumber" runat="server" CssClass="edittxt" autocomplete="off" placeholder="请输入身份证号"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="editname">医保卡号</td>
                <td>
                    <asp:TextBox ID="txtLicense" runat="server" CssClass="edittxt" autocomplete="off" placeholder="请输入医保卡号"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="editname">家庭地址</td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="edittxt" autocomplete="off" placeholder="请输入家庭地址"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="editname">新的密码</td>
                <td>
                    <asp:TextBox ID="txtPassword1" runat="server" CssClass="edittxt" TextMode="Password" placeholder="不修改密码不用填写"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="editname">确认密码</td>
                <td>
                    <asp:TextBox ID="txtPassword2" runat="server" CssClass="edittxt" TextMode="Password" placeholder="不修改密码不用填写"></asp:TextBox>
                </td>
            </tr>
        </table>
        
        <div class="btnDiv">
            <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btnDel" OnClick="btnSave_Click" style="background:#F96410" />
            <div style="height:10px;"></div>
            <asp:Button ID="btnClear" runat="server" Text="注销" CssClass="btnDel" OnClick="btnClear_Click" OnClientClick="{return confirm('确定注销吗？');}" />
        </div>

        <div id="attention-dialog" class="attention-layout"></div>

    </form>
 
</body>

</html>