﻿<%@ page language="C#" autoeventwireup="true" inherits="Express_Search, App_Web_mks4b3mh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="shortcut icon" href="../Images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="../Skins/bootstrap/css/bootstrap.min.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding:10px; line-height:2em;">
        <div id="InfoDiv" runat="server"></div>
    </div>
    </form>
</body>
</html>
