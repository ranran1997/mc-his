﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Order_SelectItem, App_Web_nrpmib2t" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
    
    <script type="text/javascript">

        function openPanel(id, img, productName, number, model, price, unit, qty, remark) {
            document.getElementById("ctl00_cphMain_txtID").value = id;
            document.getElementById("ctl00_cphMain_txtImagePath").value = img;
            document.getElementById("ctl00_cphMain_txtProductName").value = productName;
            document.getElementById("ctl00_cphMain_txtNumber").value = number;
            document.getElementById("ctl00_cphMain_txtModel").value = model;
            document.getElementById("ctl00_cphMain_txtPrice").value = price;
            document.getElementById("ctl00_cphMain_txtUnit").value = unit;
            document.getElementById("ctl00_cphMain_txtQty").value = qty;
            document.getElementById("ctl00_cphMain_txtRemark").value = remark;
        }

    </script>

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            <asp:Label ID="lblTitle" runat="server" Text="选择药品"></asp:Label>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">选择药品</li>
          </ol>
        </section>

        <section class="content">
            
          <div id="AlertDiv" runat="server"></div>

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" SortExpression="pk_Product" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Product") %>'></asp:Label>
                                    <asp:Label ID="lblUser" runat="server" Text='<%# Bind("fk_User") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblImagePath" runat="server" Text='<%# Bind("ImagePath") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="lblThumbnail" runat="server" Text='<%# Bind("ImagePath") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="药品分类" SortExpression="CategoryName">
                                <ItemTemplate>
                                    <asp:Label ID="lblCategoryName" runat="server" Text='<%# Bind("CategoryName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="药品名称">
                                <ItemTemplate>
                                    <asp:Label ID="lblProductName" runat="server" Text='<%# Bind("ProductName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="220px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="药品编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblNumber" runat="server" Text='<%# Bind("Number") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="型号">
                                <ItemTemplate>
                                    <asp:Label ID="lblModel" runat="server" Text='<%# Bind("Model") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="客户单价">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerPrice" runat="server" Text='<%# Bind("CustomerPrice") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="库存数量">
                                <ItemTemplate>
                                    <asp:Label ID="lblQty" runat="server" Text='<%# Bind("Qty") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="单位">
                                <ItemTemplate>
                                    <asp:Label ID="lblUnit" runat="server" Text='<%# Bind("Unit") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="负责人">
                                <ItemTemplate>
                                    <asp:Label ID="lblFullName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="创建时间" SortExpression="CreateDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态" SortExpression="StatusID">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvAdd" runat="server" CssClass="fancybox link-black text-sm" NavigateUrl="#MemoDiv"><span class="label label-primary"><i class="fa fa-plus"></i> 选择该药品</span></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    <div id="pager">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                </div>
                
              </div>
            </div>
          </div>

        </section>

      </div>
    
      <div id="MemoDiv" style="display:none; width:500px;">
            
          <div class="box-body">
          
              <div class="form-group">
                  <label><asp:Label ID="Label1" runat="server" Text="药品名称"></asp:Label></label>
                  <asp:TextBox ID="txtProductName" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="form-group">
                  <label><asp:Label ID="Label2" runat="server" Text="药品编号"></asp:Label></label>
                  <asp:TextBox ID="txtNumber" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="form-group">
                  <label><asp:Label ID="Label3" runat="server" Text="型号"></asp:Label></label>
                  <asp:TextBox ID="txtModel" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="form-group">
                  <label><asp:Label ID="Label4" runat="server" Text="单价"></asp:Label></label>
                  <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="form-group">
                  <label><asp:Label ID="Label5" runat="server" Text="单位"></asp:Label></label>
                  <asp:TextBox ID="txtUnit" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="form-group">
                  <label><asp:Label ID="Label6" runat="server" Text="数量"></asp:Label></label>
                  <asp:TextBox ID="txtQty" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="form-group">
                  <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" placeholder="备注" TextMode="MultiLine" Rows="5"></asp:TextBox>
                  <asp:TextBox ID="txtID" runat="server" style="display:none;"></asp:TextBox>
                  <asp:TextBox ID="txtImagePath" runat="server" style="display:none;"></asp:TextBox>
              </div>
                  
              <div class="pull-right">
                  <asp:LinkButton ID="lnbSave" runat="server" CssClass="btn btn-primary" onclick="lnbSave_Click">新增</asp:LinkButton>
              </div>

          </div>

      </div>

</asp:Content>