﻿<%@ page language="C#" masterpagefile="~/Commons/Simple.master" autoeventwireup="true" inherits="Workflow_Sign, App_Web_3nnyqhi4" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSimple" Runat="Server">

      <div class="content-wrapper-simple">

        <section class="content">

          <div class="row">
            <div class="col-xs-12">
              <div class="box">

                <div class="box-header with-border">
                  <h3 class="box-title">
                       <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" Height="30px"></asp:DropDownList>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="box-body no-padding">
                  
                  <div class="box-body">
                        <div class="row">
                        
                            <div id="ImageDiv" runat="server"></div>

                        </div>
                    </div>
                    
                    <div class="box-footer no-padding" style="margin-top:-10px; border:0px;">
                      <div class="mailbox-controls">
                  
                        <div id="pager" style="background:#fff; border:0px; margin-top:0px; padding:2px;">
                           <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                        </div>
        
                      </div>
                    </div>

                </div>

              </div>
            </div>
          </div>

        </section>

      </div>

      <div id="BgDiv" style="width:100%; height:100%; background:#fff; position:absolute; top:0px; left:0px; display:none"></div>

      <div id="PasswordDiv" style="position:absolute; top:120px; left:50%; width:380px; height:100px; background:#fff; margin:0 auto 0 -200px; padding:10px; display:none">
        <table>
            <tr>
                <td>
                    请输入密码：
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" style="width:180px"></asp:TextBox>
                    <asp:TextBox ID="txtID" runat="server" style="display:none"></asp:TextBox>
                    <asp:TextBox ID="txtUrl" runat="server" style="display:none"></asp:TextBox>
                    <asp:TextBox ID="txtType" runat="server" style="display:none"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnOK" runat="server" Text="确定" style="width:80px;" onclick="btnOK_Click" />
                </td>
            </tr>
        </table>
      </div>
      
        <script type="text/javascript">

            function InsertImage(imgUrl) {
                window.parent.insertContent('<img src="' + imgUrl + '" style="border:0" />');
                window.parent.$.fancybox.close();
            }

            function InsertSignature(id, imgUrl) {
                window.parent.document.getElementById('Signature').innerHTML = '<a href="' + imgUrl + '" class="fancybox fancybox.image" data-fancybox-group="gallery"><img src="' + imgUrl + '" style="border:0; height:50px; margin-left:20px" /></a>';
                window.parent.document.getElementById('ctl00_cphMain_txtID').value = id;
                window.parent.$.fancybox.close();
            }

            function SetUrl(id, url, type) {
                document.getElementById('ctl00_cphSimple_txtID').value = id;
                document.getElementById('ctl00_cphSimple_txtUrl').value = url;
                document.getElementById('ctl00_cphSimple_txtType').value = type;
                document.getElementById('BgDiv').style.display = "";
                document.getElementById('PasswordDiv').style.display = "";
            }

        </script>

</asp:Content>