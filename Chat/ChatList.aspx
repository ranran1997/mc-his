﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_ChatList, App_Web_4p0w1hca" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <%=MainCss%>
    <script src="JS/jquery.min.js"></script>
    <script src="JS/jquery.lazyload.min.js"></script>
    <script src="JS/mojocube.js?v=1"></script>
</head>

<body style="padding:0px; margin:0px; background:#E6E5E5; overflow-y:auto; overflow-x:hidden">
    <form id="form1" runat="server">
        
        <asp:ScriptManager id="ScriptManager1"  runat="server"></asp:ScriptManager>
    
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        
               <script type="text/javascript" >
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_endRequest(function(){
                        $("img.lazy").lazyload();
                    });
                </script>
             
                <asp:Timer id="Timer1" runat="server" Interval="5000" OnTick="Timer1_Tick">
                </asp:Timer>
        
                <div id="ListDiv" runat="server">
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        
        <script type="text/javascript">
            $(function () {
                $("img.lazy").lazyload();
            });
        </script>

    </form>
</body>
</html>
