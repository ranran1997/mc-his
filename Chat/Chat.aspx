﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_Chat, App_Web_4p0w1hca" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <%=MainCss%>
    <link href="Icon/Ali1/iconfont.css" rel="stylesheet" />
    <script src="JS/jquery.min.js"></script>
    <script src="JS/mojocube.js?v=1"></script>
    
	<script type="text/javascript" src="../JS/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="../JS/Fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    
    <script type="text/javascript">

        $(document).ready(function () {
            scrollWindow();

            $(".fancybox").fancybox({
                parent: "form:first"
            });
        });

        function scrollWindow() {
            $(".container").scrollTop($(".container")[0].scrollHeight);
        }

        function sendMsg() {
            __doPostBack("lnbSend", "");
        }

        var down = 0;
        function enter() {
            if ((window.event.keyCode == 13) && (window.event.ctrlKey))
            {
                down = 1;
            }
            if (window.event.keyCode == 13) {
                if (down == 1) {
                    document.getElementById("txtContent").value += '\n';
                    down = 0;
                } else {
                    __doPostBack("lnbSend2", "");
                }
            }
        }

    </script>

</head>
<body style="padding:0px; margin:0px; background:#F5F5F5; overflow:hidden">
    <form id="form1" runat="server">
        
        <asp:ScriptManager id="ScriptManager1"  runat="server"></asp:ScriptManager>
    
        <div class="top">
            <div style="padding:0px 15px; line-height:50px; font-size:13pt;">
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </div>
        </div>

        <div class="container">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
        
                    <asp:Timer id="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                    </asp:Timer>
        
                    <div id="ChatContent" runat="server" style="padding:20px 30px;">
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSend" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>

        </div>
        
        <div class="foot">

            <div style="padding:5px 10px; height:30px;">
                
                <div class="icon_bar">
                    <a href="Emoji.aspx" class="icon_btn dropdown-toggle fancybox fancybox.iframe" title="发送表情"><i class="icon iconfont">&#xf00f5;</i></a>
                </div>

                <div class="icon_bar">
                    <a href="Upload.aspx" class="icon_btn dropdown-toggle fancybox fancybox.iframe" title="发送附件"><i class="icon iconfont">&#xf00df;</i></a>
                </div>

                <div class="icon_bar">
                    <asp:HyperLink ID="hlHistory" runat="server" CssClass="icon_btn dropdown-toggle fancybox fancybox.iframe" title="历史记录"><i class="icon iconfont">&#xf016c;</i></asp:HyperLink>
                </div>

            </div>
            
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
        
                    <div style="padding:10px 20px">
                        <asp:TextBox ID="txtContent" runat="server" TextMode="MultiLine" style="width:100%; border:0px; height:90px; outline:none; font-size:11pt;" onKeyUp="enter()"></asp:TextBox>
                    </div>

                    <div style="padding:10px; text-align:right">
                        <asp:Button ID="btnSend" runat="server" Text="发送" style="width:80px;height:30px; background:#F5F5F5; border:solid 1px #ddd" OnClick="btnSend_Click" />
                    </div>

                    <div style="display:none">
                        <asp:TextBox ID="txtFile" runat="server"></asp:TextBox>
                        <asp:LinkButton ID="lnbSend" runat="server" OnClick="lnbSend_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnbSend2" runat="server" OnClick="btnSend_Click"></asp:LinkButton>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>

    </form>
</body>
</html>
