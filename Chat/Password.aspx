﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_Password, App_Web_4p0w1hca" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <%=MainCss%>
</head>
<body style="background:#F5F5F5">
    <form id="form1" runat="server">
        
        <div class="top">
            <div style="padding:0px 15px; line-height:50px; font-size:13pt;">
                密码设置
            </div>
        </div>

        <div style="padding-top:150px;">
    
            <table class="reg-tb">
                <tr>
                    <td class="reg-name">
                        原始密码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="textbox" TextMode="Password" placeholder="请输入原始密码"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="reg-name">
                        修改密码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword1" runat="server" CssClass="textbox" TextMode="Password" placeholder="请输入新的密码"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="reg-name">
                        确认密码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword2" runat="server" CssClass="textbox" TextMode="Password" placeholder="请确认新的密码"></asp:TextBox>
                    </td>
                </tr>
            </table>
            
            <div style="padding:10px; text-align:center">
                <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" style="width:100px; padding:5px; background:#E9E9E9; border:solid 1px #ddd" />
            </div>
            
            <div style="padding:10px; text-align:center">
                <asp:Label ID="lblInfo" runat="server"></asp:Label>
            </div>

        </div>

    </form>
</body>
</html>